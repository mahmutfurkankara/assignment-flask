from flask import Flask, request, render_template, jsonify

app = Flask(__name__)


@app.route('/', methods=['POST', 'GET'])
def hello_world():
    message = None
    content = {'message': None, 'output': None}
    if request.method == 'POST':
        letter_data = doBackendStuff(name=request.form["input1"])
        message = letter_data["error"]
        input_name = letter_data["name"]
        if input_name is None:
            content = {'message': message, 'output': None}
            return render_template('index.html', **content)
        integer_data = request.form["input2"]
        output = surprize(input_name, integer_data)
        content = {'message': message,'output': output}
    return render_template('index.html', **content)


def surprize(name, number):
    output = ""
    array = list(number)
    for letter in name:
        index = name.index(letter)
        for count in range(int(number[index])):
            output += letter
    return output


def doBackendStuff(name):
    name = name.replace(' ','')
    required_letter_number = 5
    length = len(name)
    error = None
    if (length == required_letter_number) and (name.isalpha()):
        context = {'name': name, 'error': error, 'length': length}
        return context
    else:
        error = 'Wrong Input '
        context = {'name': None, 'error': error, 'length': length}
        return context


if __name__ == '__main__':
    app.run(debug=True)
